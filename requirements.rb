# GEMS
require 'curb'
require 'socket'
require 'colorize'
require 'json'

require 'poloniex'
require 'hitbtc'
require 'bittrex'


# FILES
require "./conf/apiconf.rb"
require "./method/data.rb"
require "./method/action.rb"

# MODULES
include Apiconf


=begin
###############################################################################
Logic:
- first get ticker (keep getting it every 15s)
- IF difference >> sth (ideally trade fee) THEN get trade balances
- IF some DO the trade with 20% of it
###############################################################################
=end

# CHECK if keys set
# make it better

=begin
  puts "Checking exchanges with API data..."
   ALL.each do |exchange, key|
 	puts "- exchange #{exchange} - #{if key.empty? then "inactive" else "probably active?".green end}"
  end
=end
