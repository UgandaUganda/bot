# get Data
# only puts and compare no trade

# GEMS
require 'curb'
require 'socket'
require 'colorize'
require 'csv'

# FILES
require "./conf/apiconf.rb"
require "./method/data.rb"
require "./method/action.rb"

# MODULES
include Apiconf


# DO

puts "Checking exchanges with API data...".yellow
ALL.each do |exchange, key|
        puts "- exchange #{exchange} - #{if key.empty? then "inactive" else "ACTIVE" end}"
end

