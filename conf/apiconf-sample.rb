module Apiconf
ALL = { 
	bitrex: {}, 
	hitbtc: {key: 0, secret: 0}, 
	poloniex: {}
      }
end
# --------- usage ------------ #

include Apiconf
# this is primitive and doesnt test the keys at all 
ALL.each do |exchange, key|
	puts "Exchange #{exchange} - #{if key.empty? then "Inactive" else "Active" end}"
end
