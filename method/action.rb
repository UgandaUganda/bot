=begin
Logic
- pull trade balance of all 3
- pull ltcbtc price or whatever
- // maybe? // check if that price was similar for some secs back?
- if diff > sthg buy where cheap and sell where dear
=end
class Sniffer
  attr_accessor :maxbid, :minask
  def max(hash)
    max = hash.max_by{|k,v| v}
    return max
  end
  def min(hash)
    min = hash.min_by{|k,v| v}
    return min
  end

  def diff(max, min)
    @maxbid = max[1].to_i
    @minask = min[1].to_i
    puts "Am I sane? => #{@maxbid} - #{@minask} | #{max[0]} vs #{min[0]}".blue
    diff = @maxbid - @minask
    return diff
  end
end
