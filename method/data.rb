class Helper
  require 'date'
  require 'digest'
  require 'openssl'
  require 'rest-client'
  @@k = "void key"
  @@s = "void secret"
  attr_accessor :name
  def initialize(name)
		@name = name
	end
  def nonce # random integer
    int = DateTime.now.strftime('%s')
  end
  def key   # api key
    ALL.each do |exchange, key|
     if ( exchange.to_s == @name ) then @@k = key[:key]; @@s = key[:secret] end ##is this ok as class var???
    end
    return @@k
  end
  def secret
    ALL.each do |exchange, key|
     if ( exchange.to_s == @name ) then @@s = key[:secret] end ##do i need it in key
    end
    return @@s
  end
end#Helper

class Pairs
  @@active = "LTC_BTC" # LTCBTC
  def self.active
		@@active
	end
  def switch_pair ( nu )#change the class var accesible without instantiating? necessary or not?
    @@active = nu
  end
end#Pairs

class Sanitize
  def do( val )
    val = val.to_f
    # if no way then return bla bla..
    val = val * 1000000000000
    val = val.to_i
    return val
  end
end
