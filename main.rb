# Main bot

require "./requirements.rb"

# Define currency Pair

pair = Pairs.active
	pair_nohyph = pair.sub "_", ""
	pairsplit = pair.split("_")
		ppair = pairsplit[1] + "_"  + pairsplit[0]  # => "BTC_LTC"
	pair_hyph = ppair.sub "_", "-"
puts "================================================================"
puts "Now checking values for #{pair} ( = #{pair_nohyph}, #{ppair}, #{pair_hyph} ): "
puts "================================================================"

# ========================================================>

# POLO GET BALANCES
# reference: https://github.com/Lowest0ne/poloniex

polo = Helper.new ("poloniex")
	begin
		Poloniex.setup do | config |
		    config.key = polo.key
		    config.secret = polo.secret
		end
		ticker_polo = Poloniex.ticker
		balances_polo = Poloniex.balances
			ticker_polo = JSON.parse(ticker_polo)
			balances_polo = JSON.parse(balances_polo)
		puts "Poloniex, #{ppair}".white.on_blue  #no no no
			last_polo = ticker_polo[ppair]["last"]
			loask_polo = ticker_polo[ppair]["lowestAsk"]
			hibid_polo = ticker_polo[ppair]["highestBid"]
	  puts " = Last trade #{last_polo}"
		puts " = Lowest ask #{loask_polo}"
		puts " = Highest bid #{hibid_polo}"
		puts "Trade balance (BTC): #{balances_polo["BTC"]}"
	rescue => msg
    puts "It is dead Jim: #{msg}".red
	end

# ========================================================>

# HITBTC GET BALANCES
# reference: https://github.com/jhk753/hitbtc_ruby

hitb = Helper.new ("hitbtc")

	begin
		hitbtc = Hitbtc::Client.new(hitb.key, hitb.secret)
		balances_hitb = hitbtc.balance("BTC")
		ticker_hitb = hitbtc.ticker(pair_nohyph)
		puts "HitBTC, #{pair}".white.on_blue  #no no no
			last_hitb = ticker_hitb.last
			loask_hitb = ticker_hitb.ask
			hibid_hitb = ticker_hitb.bid
	  puts " = Last trade #{last_hitb}"
		puts " = Lowest ask #{loask_hitb}"
		puts " = Highest bid #{hibid_hitb}"
		puts "Trade balance (BTC): #{balances_hitb.cash} (reserved #{balances_hitb.reserved})"
	rescue => msg
    puts "It is dead Jim: #{msg}".red
	end


# ========================================================>

# BITTREX GET BALANCES
# reference: https://github.com/mthjn/bittrex

bitr = Helper.new ("bitrex")
		Bittrex.config do |c|
		  c.key = bitr.key
		  c.secret = bitr.secret
		end
		begin
			brq = Bittrex::Quote.current(pair_hyph)
=begin guy could have written some docs
			balances_bitr =  Bittrex::Wallet.all
			puts balances_bitr.inspect
=end
			puts "Bittrex, #{brq.market}".white.on_blue
				last_bitr = brq.last
				loask_bitr = brq.ask
				hibid_bitr = brq.bid
			puts " = Last trade #{last_bitr}"
			puts " = Lowest ask #{loask_bitr}"
			puts " = Highest bid #{hibid_bitr}"
			#puts "Trade balance (BTC): #{balances_bitr.available}"
		rescue => msg
			puts "It is dead Jim: #{msg}".red
		end

# ========================================================>

# COMPARE
puts "================================================================"
puts "Comparing last price for #{pair}:"
puts "================================================================"

# WHAT IF equal

# !important https://www.ruby-forum.com/topic/48754
# floats should NOT be used to handle money
# all must be converted to interegers before being handled further

san = Sanitize.new
last_bitr = san.do(last_bitr)
	last_hitb = san.do(last_hitb)
		last_polo = san.do(last_polo)

hibid_bitr = san.do(hibid_bitr)
	hibid_hitb = san.do(hibid_hitb)
		hibid_polo = san.do(hibid_polo)

loask_bitr = san.do(loask_bitr)
	loask_hitb = san.do(loask_hitb)
		loask_polo = san.do(loask_polo)

sniffer = Sniffer.new
	lasts = {bitr: last_bitr, hitb: last_hitb, polo: last_polo}
	max_last = sniffer.max(lasts)
	puts "Highest last trade: #{max_last}"

	min_last = sniffer.min(lasts)
	puts "Lowest last trade: #{min_last}"

	his = {bitr: hibid_bitr, hitb: hibid_hitb, polo: hibid_polo}
	max_hibid = sniffer.max(his)
	puts "Highest hi bid: #{max_hibid}"

	los = {bitr: loask_bitr, hitb: loask_hitb, polo: loask_polo}
	min_loask = sniffer.min(los)
	puts "Lowest low ask: #{min_loask}"

# difference loask hibid

puts " = Bids vs asks #{pair}, vals * 1/12 = ".white.on_blue
puts "#{sniffer.diff(max_hibid, min_loask)}".blue
puts " = Last trades #{pair}, vals * 1/12 = ".white.on_blue
puts "#{sniffer.diff(max_last, min_last)}".blue


# check balances HERE FIRST
# => case when block
# calculate earning from 25% in
# check if higher than 0.25% off the trade (fee)
# suggest action
